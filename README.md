# JOKENPO

Aplicação simples que simula uma partida de Jokenpo.
Onde os resultados e as solicitações de informações serão exibidas e inseridas no console da IDE.


## Requisitos
-   Java 11 JDK
-   [Eclipse IDE](https://www.eclipse.org/downloads/) com Maven

## Inicializar projeto

1. Instale o Java e o Eclipse;
2. Abra o Eclipse e importe o projeto como Maven Projects;
3. Click no projeto com o botão direito, selecione **Run As** e selecione:
	- **JUnit Test** para executar os testes.
	- **Java Application** para iniciar a aplicação.

## Jogando o jogo

Será solicitado o nome do primeiro jogador e qual será a sua escolha (*Pedra, Papel, Tesoura ou Finalizar Jogo*).
Então será solicitados o nome e a escolha do segundo jogador.
Após isso a aplicação informara que é o vencedor da partida.
