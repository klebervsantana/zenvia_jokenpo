package br.com.zenvia.jokenpo.entidade;

import br.com.zenvia.jokenpo.enums.Opcoes;
import br.com.zenvia.jokenpo.enums.Resultado;

public class Usuario {
	
	private String nomeJogador;
	private Opcoes escolhaJogador;
	private Resultado statusJogador;
	
	public Usuario() {
		super();
	}
	
	public Usuario(String nomeJogador, Opcoes escolhaJogador) {
		super();
		this.nomeJogador = nomeJogador;
		this.escolhaJogador = escolhaJogador;
	}
	
	public String getNomeJogador() {
		return nomeJogador;
	}
	public void setNomeJogador(String nomeJogador) {
		this.nomeJogador = nomeJogador;
	}
	public Opcoes getEscolhaJogador() {
		return escolhaJogador;
	}
	public void setEscolhaJogador(Opcoes escolhaJogador) {
		this.escolhaJogador = escolhaJogador;
	}

	public Resultado getStatusJogador() {
		return statusJogador;
	}

	public void setStatusJogador(Resultado statusJogador) {
		this.statusJogador = statusJogador;
	}
}
