package br.com.zenvia.jokenpo.enums;

public enum Opcoes {
	PEDRA(" Pedra"), TESOURA (" Tesoura"), PAPEL(" Papel");
	
	private String descricao;
	
	Opcoes(String descricao) {	
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

}
