package br.com.zenvia.jokenpo.enums;

public enum Resultado {
	VENCEU(" venceu"), PERDEU(" perdeu"), EMPATE(" empataram");

	private String descricao;
	
	Resultado(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

}
