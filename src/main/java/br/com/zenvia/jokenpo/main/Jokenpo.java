package br.com.zenvia.jokenpo.main;

import java.util.Scanner;

import br.com.zenvia.jokenpo.entidade.Usuario;
import br.com.zenvia.jokenpo.enums.Opcoes;
import br.com.zenvia.jokenpo.service.JogarService;

public class Jokenpo {
	private static JogarService service = new JogarService();
	private static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		Usuario jogador1 = new Usuario();
		Usuario jogador2 = new Usuario();
		
		System.out.println("Digite o nome do primeiro jogador: ");
		jogador1.setNomeJogador(sc.next().toUpperCase());
		escolha(jogador1);
		System.out.println("Digite o nome do segundo jogador: ");
		jogador2.setNomeJogador(sc.next().toUpperCase());
		escolha(jogador2);
		
		System.out.println(service.jogando(jogador1, jogador2));

	}

	private static void escolha(Usuario jogador) {
		boolean escolhaCorreta = false;
		char opcao;
		do {
			System.out.println("\nDigite a op��o desejada.\n 1: PEDRA\n 2: PAPEL\n 3: TESOURA\n 0: FINALIZAR JOGO");
			opcao = sc.next().charAt(0);
			if (opcao == '1') {
				jogador.setEscolhaJogador(Opcoes.PEDRA);
				escolhaCorreta = true;
			} else if (opcao == '2') {
				jogador.setEscolhaJogador(Opcoes.PAPEL);
				escolhaCorreta = true;
			} else if (opcao == '3') {
				jogador.setEscolhaJogador(Opcoes.TESOURA);
				escolhaCorreta = true;
			}else if(opcao == '0') {
				System.out.println("\nFinalizado");
				escolhaCorreta = true;
			} else {
				System.err.println("Op��o invalida\n");
				escolhaCorreta = false;
			}
		} while (!escolhaCorreta);
	}

}
