package br.com.zenvia.jokenpo.service;

import br.com.zenvia.jokenpo.entidade.Usuario;
import br.com.zenvia.jokenpo.enums.Opcoes;
import br.com.zenvia.jokenpo.enums.Resultado;

public class JogarService {
	
	public String jogando(Usuario jogador1, Usuario jogador2) {
				
		if (!jogador1.getEscolhaJogador().equals(jogador2.getEscolhaJogador())) {			
			if (jogador1.getEscolhaJogador().equals(Opcoes.PEDRA)) {
				if (jogador2.getEscolhaJogador().equals(Opcoes.TESOURA)) {
					jogador1.setStatusJogador(Resultado.VENCEU);
					jogador2.setStatusJogador(Resultado.PERDEU);
				} else {
					jogador1.setStatusJogador(Resultado.PERDEU);
					jogador2.setStatusJogador(Resultado.VENCEU);
				}
			} 
			if (jogador1.getEscolhaJogador().equals(Opcoes.TESOURA)) {
				if (jogador2.getEscolhaJogador().equals(Opcoes.PAPEL)) {
					jogador1.setStatusJogador(Resultado.VENCEU);
					jogador2.setStatusJogador(Resultado.PERDEU);
				} else {
					jogador1.setStatusJogador(Resultado.PERDEU);
					jogador2.setStatusJogador(Resultado.VENCEU);
				}	
			}
			if (jogador1.getEscolhaJogador().equals(Opcoes.PAPEL)) {
				if (jogador2.getEscolhaJogador().equals(Opcoes.PEDRA)) {
					jogador1.setStatusJogador(Resultado.VENCEU);
					jogador2.setStatusJogador(Resultado.PERDEU);
				} else {
					jogador1.setStatusJogador(Resultado.PERDEU);
					jogador2.setStatusJogador(Resultado.VENCEU);
				}
			}
		} else {
			jogador1.setStatusJogador(Resultado.EMPATE);
			jogador2.setStatusJogador(Resultado.EMPATE);
		}
		return tratarMsgFinal(jogador1, jogador2);
	}
	
	private String tratarMsgFinal(Usuario jogador1,Usuario jogador2) {
		String resultado = "";
		resultado = jogador1.getNomeJogador()+ ":" + jogador1.getEscolhaJogador().getDescricao()+"\n";
		resultado += jogador2.getNomeJogador()+ ":" + jogador2.getEscolhaJogador().getDescricao()+"\n";
		
		if (jogador1.getStatusJogador().equals(jogador2.getStatusJogador())) {
			resultado += jogador1.getNomeJogador() + " e " + jogador2.getNomeJogador() + Resultado.EMPATE.getDescricao() + " com " + jogador1.getEscolhaJogador().name() + "\n";
		}else if (jogador1.getStatusJogador().equals(Resultado.VENCEU)) {
			resultado += jogador1.getNomeJogador() + jogador1.getStatusJogador().getDescricao() + " com " + jogador1.getEscolhaJogador().name() + "\n";
		} else {
			resultado += jogador2.getNomeJogador() + jogador2.getStatusJogador().getDescricao() + " com " + jogador2.getEscolhaJogador().name() + "\n";
			
		}
		return resultado;
	}
}
