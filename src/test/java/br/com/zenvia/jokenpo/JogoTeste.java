package br.com.zenvia.jokenpo;

import java.util.Random;

import org.junit.Test;

import br.com.zenvia.jokenpo.entidade.Usuario;
import br.com.zenvia.jokenpo.enums.Opcoes;
import br.com.zenvia.jokenpo.service.JogarService;

public class JogoTeste {

	@Test
	public void Jogador1GanhaT() {
		JogarService service = new JogarService();
		
		Usuario jogador1 = new Usuario("Carlos".toUpperCase(), Opcoes.PEDRA);
		Usuario jogador2 = new Usuario("Kleber".toUpperCase(), Opcoes.TESOURA);
		
		System.out.println(service.jogando(jogador1, jogador2));
				
	}

	@Test
	public void Jogador1PerdeT() {
		JogarService service = new JogarService();
		
		Usuario jogador1 = new Usuario("Carlos".toUpperCase(), Opcoes.PAPEL);
		Usuario jogador2 = new Usuario("Kleber".toUpperCase(), Opcoes.TESOURA);
		
		System.out.println(service.jogando(jogador1, jogador2));
				
	}
	
	@Test
	public void EmpateT() {
		JogarService service = new JogarService();
		
		Usuario jogador1 = new Usuario("Carlos".toUpperCase(), Opcoes.PAPEL);
		Usuario jogador2 = new Usuario("Kleber".toUpperCase(), Opcoes.PAPEL);
		
		System.out.println(service.jogando(jogador1, jogador2));
				
	}
	
	@Test
	public void JogoAleatorio() {
		JogarService service = new JogarService();
		
		Random gerador = new Random();
		Opcoes[] lista;
		lista = Opcoes.values();
		
		Usuario jogador1 = new Usuario("Carlos".toUpperCase(), lista[gerador.nextInt(2)]);
		Usuario jogador2 = new Usuario("Kleber".toUpperCase(), lista[gerador.nextInt(2)]);
		
		System.out.println(service.jogando(jogador1, jogador2));
				
	}
	
}
